import { defineConfig, presetIcons, presetWebFonts, presetWind, transformerDirectives, transformerVariantGroup } from 'unocss'
import { breakpoints } from './app/configs'

export default defineConfig({
  presets: [
    presetWind(),
    presetIcons({
      cdn: 'https://esm.sh/',
      extraProperties: {
        'display': 'inline-block',
        'vertical-align': 'middle',
      },
    }),
    presetWebFonts({
      provider: 'google',
      fonts: {
        sans: [],
        serif: [],
        mono: [],
      },
    }),
  ],
  transformers: [
    transformerDirectives(),
    transformerVariantGroup(),
  ],
  theme: {
    breakpoints,
    colors: {
      n: {
        primary: 'rgb(var(--n-primary))',
        info: 'rgb(var(--n-info))',
        success: 'rgb(var(--n-success))',
        warning: 'rgb(var(--n-warning))',
        error: 'rgb(var(--n-error))',
      },
    },
  },
})
