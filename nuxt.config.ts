import process from 'node:process'
import { generateAntdColorThemes } from '@bg-dev/nuxt-naiveui/utils'
import defu from 'defu'
import themeDark from './app/assets/naive-ui-theme-dark.json'
import themeLight from './app/assets/naive-ui-theme-light.json'

const colorThemes = generateAntdColorThemes({
  primary: '#e85a27',
})

const themeConfig = {
  light: defu(themeLight, colorThemes.light),
  dark: defu(themeDark, colorThemes.dark),
}

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: [
    '@nuxt/eslint',
    '@vueuse/nuxt',
    '@unocss/nuxt',
    '@pinia/nuxt',
    '@bg-dev/nuxt-naiveui',
  ],

  devtools: {
    enabled: true,
  },

  runtimeConfig: {
    public: {
      // https://github.com/nuxt/nuxt/issues/15220#issuecomment-1397374835
      apiBase: process.env.NUXT_PUBLIC_API_BASE || '',
    },
  },

  future: {
    compatibilityVersion: 4,
  },
  compatibilityDate: '2024-08-05',

  nitro: {
    experimental: {
      openAPI: true,
    },
    routeRules: {
      '/api/**': { cors: true },
    },
  },

  postcss: {
    plugins: {
      'postcss-nested': {},
    },
  },

  eslint: {
    config: {
      standalone: false,
    },
  },

  naiveui: {
    themeConfig,
  },
})
