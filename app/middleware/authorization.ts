export default defineNuxtRouteMiddleware((to) => {
  const appConfig = useAppConfig()
  const token = useCookie(appConfig.cookieName.TOKEN)
  const returnUrl = useCookie(appConfig.cookieName.RETURN_URL)

  if (!token.value) {
    returnUrl.value = to.fullPath
    return navigateTo({ path: '/login' })
  }
})
