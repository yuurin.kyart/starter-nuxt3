export default defineNuxtPlugin((nuxtApp) => {
  const api = $fetch.create({
    baseURL: nuxtApp.$config.public.apiBase,
    onRequest({ options }) {
      const appConfig = useAppConfig()
      const token = useCookie(appConfig.cookieName.TOKEN).value

      if (token)
        options.headers.set('Authorization', `Bearer ${token}`)
    },
    onResponse({ response }) {
      const data = response._data ?? {}

      if (typeof data !== 'object')
        return

      const { code, message } = data

      if (typeof code !== 'number')
        return

      if (code === -1) {
        const appConfig = useAppConfig()
        const token = useCookie(appConfig.cookieName.TOKEN)
        token.value = null
      }

      if (code !== 0)
        throw new Error(message || `Unknown Error (${code})`)
    },
  })

  return {
    provide: {
      api,
    },
  }
})
