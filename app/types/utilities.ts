import type { useFetch } from '#app'

export type MaybeArray<T> = T | T[]
export type MaybeFunctionReturn<T> = T | (() => T)
export type UseFetchReturn = ReturnType<typeof useFetch>
