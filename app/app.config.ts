export default defineAppConfig({
  cookieName: {
    TOKEN: '__WEBSITE_TOKEN__',
    RETURN_URL: '__WEBSITE_RETURN_URL__',
  },
  title: 'Nuxt 3 App',
})
