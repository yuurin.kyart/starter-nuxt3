// ref: https://stackoverflow.com/questions/72041740/how-to-set-global-api-baseurl-used-in-usefetch-in-nuxt-3#75870302
export const useAPI: typeof useFetch = (url, options = {}) => {
  const { $api } = useNuxtApp()

  return useFetch(url, {
    ...options,
    $fetch: $api,
  })
}
