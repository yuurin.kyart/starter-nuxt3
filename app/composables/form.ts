import type { Fn, MaybeRefOrGetter } from '@vueuse/core'
import { toRef } from '@vueuse/core'
import type { FormItemRule, FormValidationError, NForm } from 'naive-ui'

export type FormModel = Record<string, any>
export type FormModelRules<T = string> = Record<keyof T, FormItemRule | FormItemRule[]>

export function useForm<T extends FormModel>(initialData: MaybeRefOrGetter<T>, rules: Partial<FormModelRules<T>> = {}) {
  const validateErrors = ref<FormValidationError[]>([])
  const formData = toRef(initialData)
  const formRef = ref<InstanceType<typeof NForm>>()
  const { cloned: formModel, sync } = useCloned(() => formData.value)
  const formError = computed<Partial<Record<keyof T, string>>>(() => Object.fromEntries(validateErrors.value.map((group) => [group[0]!.field, group[0]!.message])))

  function clear() {
    validateErrors.value = []
    formRef.value?.restoreValidation()
  }

  function resetForm() {
    clear()
    sync()
  }

  function submitForm(resolve: Fn, reject?: (errors: Partial<Record<keyof T, string>>) => void) {
    clear()

    setTimeout(() => {
      formRef.value?.validate((errors) => {
        if (errors) {
          validateErrors.value = errors
          reject?.(formError.value)
          return
        }

        validateErrors.value = []
        resolve()
      }).catch(() => {
        // promise error
      })
    }, 0)
  }

  return {
    formRef,
    formModel,
    formRules: rules as FormModelRules,
    formError,
    resetForm,
    submitForm,
  }
}
