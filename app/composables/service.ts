import { destr } from 'destr'
import type { FetchContext, FetchResponse } from 'ofetch'
import { computed } from 'vue'
import { useFetch } from '#app'
import type { MaybeArray, UseFetchReturn } from '~/types'

function onServiceRequest({ options, request }: FetchContext) {
  const { responseType = 'json' } = options

  const parseResponseJson = (responseText: string) => {
    const text = responseText.trim()

    if (import.meta.env.DEV && responseText !== text)
      console.warn('[%o] responseText auto fix', request)

    return destr(text)
  }

  if (responseType === 'json')
    options.parseResponse = parseResponseJson

  const appConfig = useAppConfig()
  const runtimeConfig = useRuntimeConfig()
  const token = useCookie(appConfig.cookieName.TOKEN).value

  options.baseURL = runtimeConfig.public.apiBase
  // options.baseURL = ''

  if (token)
    options.headers.set('Authorization', `Bearer ${token}`)
}

function afterResponse(response: FetchResponse<any>) {
  const data = response._data ?? {}

  if (typeof data !== 'object')
    return

  const { code, message } = data

  if (typeof code !== 'number')
    return

  if (code === -1) {
    const appConfig = useAppConfig()
    const token = useCookie(appConfig.cookieName.TOKEN)
    token.value = null
  }

  if (code !== 0)
    throw new Error(message || `未知錯誤(${code})`)
}

export const $service: typeof $fetch = $fetch.create({
  onRequest: onServiceRequest,
  onResponse: ({ response }) => afterResponse(response),
})

// ref: https://stackoverflow.com/questions/72041740/how-to-set-global-api-baseurl-used-in-usefetch-in-nuxt-3#75870302
export const useService: typeof useFetch = (path, options = {}) => {
  options.onRequest ??= onServiceRequest
  options.onResponse ??= ({ response }) => afterResponse(response)

  return useFetch(path, options)
}

// ref: https://github.com/nuxt/nuxt/issues/18810#issuecomment-1563698089
export function isServiceLoading(fetchInstance: MaybeArray<UseFetchReturn>) {
  const isStatusPending = (instance: UseFetchReturn) => instance.status.value === 'pending'

  if (Array.isArray(fetchInstance))
    return fetchInstance.some((i) => isStatusPending(i))

  return isStatusPending(fetchInstance)
}

export function useServiceLoading(fetchInstance: MaybeArray<UseFetchReturn>) {
  const isLoading = computed(() => isServiceLoading(fetchInstance))

  return {
    isLoading,
  }
}
