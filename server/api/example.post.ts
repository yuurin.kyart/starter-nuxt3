import { z } from 'zod'

defineApiRouteMeta('/api/example', {
  method: 'post',
  summary: 'DO BY POST',
  description: '# DO BY POST',
  requestBody: {
    content: {
      'application/json': {
        schema: toExampleSchema({
          id: 1,
        }, {
          id: 'Edit id',
        }),
      },
    },
  },
  response: toExampleSchema({
    id: 1,
    name: 'foo',
    description: 'about foo',
  }, {
    id: 'ID',
    name: 'Name',
    description: 'Description',
  }),
})

const bodySchema = z.object({
  id: z.coerce.number().min(1),
})

export default defineEventHandler(async (event) => {
  const query = await readValidatedBody(event, bodySchema.safeParse)

  if (query.success) {
    return createApiResponse({
      ...query.data,
      name: 'name',
      description: 'description',
    })
  }

  return createApiResponse(null, { code: 1, message: query.error.issues[0].message })
})
