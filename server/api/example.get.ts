import { toExampleSchema } from '@byyuurin/nitro-openapi'
import { z } from 'zod'

defineApiRouteMeta('/api/example', {
  summary: 'DO BY GET',
  description: '# DO BY GET',
  parameters: [
    { name: 'keyword', in: 'query', schema: { type: 'string' } },
  ],
  response: toExampleSchema({ keyword: 'hello' }),
})

const querySchema = z.object({
  keyword: z.string({ required_error: 'keyword is required.' }),
})

export default defineEventHandler(async (event) => {
  const query = await getValidatedQuery(event, querySchema.safeParse)

  if (query.success)
    return createApiResponse(query.data)

  return createApiResponse(null, { code: 1, message: query.error.issues[0].message })
})
