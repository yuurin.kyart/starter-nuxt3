import type { InternalApi } from 'nitropack'
import type { configExtends } from '../plugins/swagger'
import { defineOperation, register } from '../plugins/swagger'
import type { ApiJsonModel, ApiJsonResponse, ApiRouteMeta } from '../types'

export { defineOperation } from '../plugins/swagger'
export { resolveSchemaObject, toExampleSchema } from '@byyuurin/nitro-openapi'

export function createApiResponse<DataT = undefined>(
  data?: DataT,
  options: Partial<Omit<ApiJsonModel, 'data'>> = {},
): ApiJsonResponse<ApiJsonModel<DataT>> {
  const codeMessage = new Map([
    [-1, '金鑰失效'],
    [0, '回應正常'],
    [1, '其他錯誤'],
  ])

  const { code = 0 } = options
  const message = `[MOCK] ${options.message ?? (codeMessage.get(code) || '未知錯誤')}`
  return { code, message, data } as any
}

export function defineApiRouteMeta(
  route: keyof InternalApi,
  meta: ApiRouteMeta<typeof configExtends> = {},
) {
  const { method = 'get', response, responses, ...defaults } = meta

  if (response)
    response.description ??= '回傳內容'

  const operation = defineOperation({
    ...defaults,
    responses: {
      ...responses,
      ...response === false
        ? {}
        : {
            200: {
              description: 'OK',
              content: {
                'application/json': {
                  schema: {
                    type: 'object',
                    properties: {
                      code: { type: 'number', description: '狀態碼', example: 0 },
                      message: { type: 'string', description: '狀態說明', example: 'success' },
                      ...response ? { data: response } : {},
                    },
                  },
                },
              },
            },
          },

    },
  })

  register(route, operation, method)
}
