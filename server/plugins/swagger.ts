import { createOpenApiRegister } from '@byyuurin/nitro-openapi'
import type { OpenAPI3 } from 'openapi-typescript'
import * as schemas from '../schemas'

const description = `
## 相關連結

## 開發備忘

| API 狀態碼                                                  |
| ----------------------------------------------------------- |
| \`-1\` 金鑰失效, \`0\` 回應正常, \`1\` 其他錯誤               |
`

export const { register, merge, defineOperation, configExtends } = createOpenApiRegister({
  info: {
    title: 'API 文件',
    version: 'dev',
    description,
  },
  servers: [
  ],
  tags: [
    { name: 'API Routes', description: 'Work in progress' },
    { name: 'Internal', description: 'Internal APIs' },
  ] as const,
  components: {
    securitySchemes: {
      UserToken: {
        type: 'http',
        description: 'JWT Token',
        scheme: 'bearer',
      },
    },
    schemas,
  },
})

export default defineNitroPlugin((nitro) => {
  nitro.hooks.hook('beforeResponse', (event, context) => {
    // config adjust
    if (event.path.endsWith('swagger')) {
      let body = context.body as string

      const settings = Object.entries({
        docExpansion: 'list', // expand all group
        // defaultModelsExpandDepth: -1, // hide schemas
      }).map(([k, v]) => `$1${k}: ${typeof v === 'string' ? `"${v}"` : JSON.stringify(v)}`)

      body = body.replace(/(\s*)(url:\s"[^"]+")/, `${settings},$1$2`)
      // console.log(body.match(/(?:SwaggerUIBundle\(([^;]+)\))/)?.[1])

      // wait for Nitro update
      body = body.replace(/swagger-ui-dist@\^4/g, 'swagger-ui-dist@^5')

      context.body = body
      return
    }

    if (!/openapi.json/.test(event.path))
      return

    // merge config
    const config = merge(context.body as OpenAPI3)

    config.openapi = '3.0.3'

    const { paths = {} } = config

    for (const path in paths) {
      Object.entries(paths[path]).forEach(([method, operation]) => {
        if ('tags' in operation) {
          paths[path][method as any] = {
            ...operation,
            tags: operation.tags.slice(-1),
          }
        }
      })
    }

    context.body = config
  })
})
