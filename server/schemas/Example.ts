export interface Example {
  num: number
  boo: boolean
  str: string
}

export default toExampleSchema<Example>({
  num: 1,
  boo: true,
  str: 'string',
}, {
  num: 'example number',
  boo: 'example boolean',
  str: 'example string',
})
