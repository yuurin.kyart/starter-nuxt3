import type {
  ApiRegisterOptions,
  MaybeReference,
  OperationType,
  PathOperation,
  ReferenceType,
  SchemaType,
} from '@byyuurin/nitro-openapi'

export interface ApiJsonModel<T = undefined> {
  /**
   * 狀態碼
   */
  code: number
  message: string
  data: T
}

export type ApiJsonResponse<T extends ApiJsonModel<unknown>> = Omit<T, T['data'] extends undefined ? 'data' : ''>

export type ApiRouteMeta<T extends ApiRegisterOptions = ApiRegisterOptions> = OperationType<T> & {
  /** @default "get" */
  method?: keyof PathOperation
  response?: MaybeReference<SchemaType<ReferenceType<T>>, ReferenceType<T>> | false
}
