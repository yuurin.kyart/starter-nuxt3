// @ts-check
import byyuurin from '@byyuurin/eslint-config'
import withNuxt from './.nuxt/eslint.config.mjs'

export default byyuurin(
  {
    formatters: {
      prettierOptions: {
        singleQuote: false,
      },
    },
  },
  // @ts-expect-error missing type
  withNuxt({
    rules: {
      'nuxt/nuxt-config-keys-order': 'warn',
    },
  }),
)
